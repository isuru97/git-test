import { Component, OnInit, createPlatformFactory, ViewChild } from '@angular/core';
import { FormBuilder, Validator, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import { flyInOut, expand } from '../animation/app-animation';
import { FeedbackService } from '../services/feedback.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display:block'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class ContactComponent implements OnInit {
  feedbackForm: FormGroup;
  feedback: Feedback;
  contactType = ContactType;
  feedbackSubmited = false;
  confirmFeedback: Feedback;
  confirmFeedbackDone = false;

  @ViewChild('fform') feedbackFormDrective;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  };

  validationMessages = {
    'firstname': {
      'required': 'First Name is requierd.',
      'minLenght': 'First Name must be 2 characters.',
      'maxlenght': 'First Name cannot be more than 25 characters.',
    },
    'lastname': {
      'required': 'Last Name is requierd.',
      'minlenght': 'Last Name must be 2 characters.',
      'maxlenght': 'Last Name cannot be more than 25 characters.',
    },
    'telnum': {
      'required': 'Tel. Number is requierd.',
      'pattern': 'Tel Number can be contain only numbbers.',
    },
    'email': {
      'required': 'Tel. Number is requierd.',
      'email': 'Email not in valid format.',
    },
  };


  constructor(
    private fb: FormBuilder,
    private feedbackService: FeedbackService
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname: ['', Validators.required],
      telnum: [0, [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: ['', Validators.required],
      contacttype: ['None', Validators.required],
      message: ['', Validators.required]
    });
    this.feedbackForm.valueChanges.subscribe(
      data => this.onValueChanged(data)
    );
    this.onValueChanged();
  }

  onSubmit() {
    this.feedbackSubmited = true;
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    // submit data
    this.feedbackService.submitFeedback(this.feedback)
      .subscribe(
        data => {
          this.feedbackSubmited = false;
          this.confirmFeedbackDone = true;
          // this.dish = dish; this.dishCopy = dish;
          this.confirmFeedback = data;
          setTimeout(() => {
            this.confirmFeedbackDone = false;
          }, 5000);

          console.log(data);
        },
        errmess => {
          console.log(errmess.error);
        }
      );


    this.feedbackForm.reset({
      firstname: '',
      lastname: '',
      telnum: 0,
      email: '',
      agree: ['', Validators.required],
      contacttype: ['None', Validators.required],
      message: ['', Validators.required]

    });
    this.feedbackFormDrective.resetForm();
  }
  onValueChanged(data?: any) {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }


}
