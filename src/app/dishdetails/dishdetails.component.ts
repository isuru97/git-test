import { Component, OnInit, Input } from '@angular/core';
import { Dish } from '../shared/dish';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { visibility, flyInOut, expand } from '../animation/app-animation';

@Component({
  selector: 'app-dishdetails',
  templateUrl: './dishdetails.component.html',
  styleUrls: ['./dishdetails.component.css'],
  host: { '[@flyInOut]': 'true', 'style': 'display:block' },
  animations: [
    flyInOut(),
    visibility(),
    expand()

  ]
})
export class DishdetailsComponent implements OnInit {
  // @Input()
  dish: Dish;
  dishIds: string[];
  prev: string;
  next: string;
  commentForm: FormGroup;
  dishCopy: Dish;
  errMess: string;
  visibility = 'shown';

  commentPrev = {
    rating: 0,
    comment: '',
    author: '',
    date: ''
  };

  formErrors = {
    'name': '',
    'comment': ''
  };

  validationMessages = {
    'name': {
      'required': 'Author Name is requierd.',
      'minlenght': 'Author Name must be at 2 characters or long.',
      'maxlenght': 'Author Name cannot be more than 25 characters.'
    },
    'comment': {
      'required': 'comment is requierd.'
    }
  };

  constructor(
    private dishService: DishService,
    private location: Location,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.createForm();
  }

  formatLabel(value: number) {
    return value;
  }
  ngOnInit() {
    this.dishService.getDishIds().subscribe(
      data => this.dishIds = data
    );
    this.route.params
      .pipe(switchMap((params: Params) => {
        this.visibility = 'hidden';
        return this.dishService.getDish(params['id']);
      }))
      .subscribe(
        dish => {
          this.dish = dish;
          this.dishCopy = dish;
          this.setPrevNext(dish.id);
          this.visibility = 'shown';
        },
        errmess => this.errMess = <any>errmess
      );
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  createForm() {
    this.commentForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      comment: ['', [Validators.required, Validators.minLength(2)]],
      rating: [5],
    });
    this.commentForm.valueChanges.subscribe(
      data => {
        this.onValueChanged(data);
      }
    );
    this.onValueChanged();
  }
  onValueChanged(data?: any) {
    this.commentPrev = {
      rating: this.commentForm.get('rating').value,
      comment: this.commentForm.get('comment').value,
      author: this.commentForm.get('name').value,
      date: Date.now().toString()
    };
    if (!this.commentForm) { return; }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.dish.comments.push(this.commentPrev);
    this.dishService.putDish(this.dishCopy)
      .subscribe(dish => {
        this.dish = dish; this.dishCopy = dish;
      },
        errmess => { this.dish = null; this.dishCopy = null; this.errMess = <any>errmess; });
    this.commentForm.reset();
    this.commentPrev = {
      rating: 5,
      comment: '',
      author: '',
      date: '',
    }
  }
}
